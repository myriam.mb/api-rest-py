### Installation

require python3
- créer un environnement virtuel :
    - $ virtualenv env -p python3
    - $ . env/bin/activate
    - (env) $ pip install flask
    - (env) $ python -m pip install python-firebase

Pour run le server :
- python app.py

Installation de apiDoc
- npm install apidoc -g

Recherche de veille
- 127.0.0.1:5000/api/v1/topics/veilles?id=0

Configapp
Dans le dossier "apirest", renommer le fichier "example-configapp.py" en "configapp.py" et remplacer les éléments par les infos de votre realtime database.
- urlFirebase pour l'url de votre database
- Pour urlApi, gardez "Veilles" à la fin de l'url

Rename
Renommer le fichier async.py qui se trouve dans env/lib/python3.8/site-packages/firebase/ en quelque chose d'autre comme par exemple asynch.py et remplacer tous les import "from .async import process_pool" dans les fichiers :
- __init__.py
- firebase.py
- decorators.py
