# To generate a new secret key:
# >>> import random, string
# >>> "".join([random.choice(string.printable) for _ in range(24)])

import string
import random

urlFirebase = ""
urlApi = "/api-flask-xxxxx-default-xxxx/Veilles"

# Get ascii Characters numbers and punctuation (minus quote characters as they could terminate string).
forkey = ''.join([string.ascii_letters, string.digits, string.punctuation]).replace('\'', '').replace('"', '').replace('\\', '')

SECRET_KEY = ''.join([random.SystemRandom().choice(forkey) for i in range(24)])

print(SECRET_KEY)