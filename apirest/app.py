# -*- coding: utf-8 -*-
from flask import Flask, jsonify, request, render_template
import configapp
from firebase import firebase


app = Flask(__name__, static_folder='static', template_folder='../apidocs')

firebase = firebase.FirebaseApplication(configapp.urlFirebase, None)


data_veilles = {
    'title': 'CSS',
    'description': 'Langage de programmation',
    'link': 'url',
    'author': 'simplon',
    'tags': 'simplon',
}
data_veilles_get = firebase.get(configapp.urlApi, '')
data_veilles_post = firebase.get(configapp.urlApi, data_veilles)

id_veille = ''
key_object = ''
put_object = ''

data_veilles_delete = firebase.get(configapp.urlApi, id_veille )
data_veilles_put = firebase.get(configapp.urlApi + id_veille, key_object)

@app.route("/")
def hello():
    return "Hello World!"

# pour ajouter et voir les veilles
@app.route('/api/v1/topics', methods=['GET', 'POST'])
def list_api():
    if request.method == "GET":
        return jsonify(data_veilles_get)
    if request.method == "POST":
        return jsonify(data_veilles_post)

# apidocs
@app.route('/api/v1/test/', methods=['OPTIONS', 'GET'])
def v1():
    return render_template('index.html')

"""
@api {get} /v1/test/ Veilles technos
@apiName Veilles technos
@apiGroup veille

@apiSuccess {Object} veilles All the presentations saved in the databaseand their Id's

@apiSuccessExample {js} Success-Response:
    HTTP/1.1 200 OK
    data_veilles = {
        'title': 'CSS',
        'description': 'Langage de programmation',
        'link': 'url',
        'author': 'simplon',
        'tags': 'simplon',
    }
"""

# voir le détail d'une veille, supprimer ou modifier
@app.route('/api/v1/topics/veilles/' + key_object, methods=['GET', 'DELETE', 'PUT'])
def api_id():
    if 'key' in request.args:
        key = key_object   

    else:
        return "Error: Pas d'id "
    info_veille = []
    for veille in data_veilles_get:
        info_veille.append(veille)
        return jsonify(info_veille)

    if request.method == "DELETE":
        return jsonify(data_veilles_delete)
    
    if request.method == "PUT":
        return jsonify(data_veilles_put)


if __name__ == "__main__":
    app.secret_key = configapp.SECRET_KEY
    app.run(debug=True)
