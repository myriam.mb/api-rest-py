define({ "api": [
  {
    "type": "get",
    "url": "/v1/test/",
    "title": "Veilles technos",
    "name": "Veilles_technos",
    "group": "veille",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "veilles",
            "description": "<p>All the presentations saved in the databaseand their Id's</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\ndata_veilles = {\n    'title': 'CSS',\n    'description': 'Langage de programmation',\n    'link': 'url',\n    'author': 'simplon',\n    'tags': 'simplon',\n}",
          "type": "js"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apirest/app.py",
    "groupTitle": "veille"
  }
] });
