define({
  "name": "Api rest en Python / Flask",
  "version": "0.1.0",
  "description": "Api rest pour lister, ajouter, modifier et supprimer des veilles technos",
  "title": "Custom apiDoc browser title",
  "url": "http://127.0.0.1:5000/",
  "header": {
    "title": "My own header title",
    "filename": ""
  },
  "footer": {
    "title": "My own footer title",
    "filename": ""
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-08-31T10:30:24.213Z",
    "url": "https://apidocjs.com",
    "version": "0.29.0"
  }
});
